# Babylon test project

Hi and thank you for reviewing the test and the read me file.

I'll try to keep this brief, please feel free to just look at key words and not read every word ;)

I've decided to create the project using **VIPER**. As I'm sure you know, it's a buzzword lately. But besides that, it's a very good pattern to use for modularity, testing, and for clean architecture, and **SOLID** principles.

I've broken the project in to three key components akin to the Babylon project readme. You have 
1. Users
2. Posts
3. Post Details 
All of which have accompanying **unit tests**.

The modules all follow the VIPER aforementioned structure.

Some views, i.e. table cells, use MVVM architecture. I didn't see the point in overkill for such small components imho

For the async, FRP, I decided to use **PromiseKit**. I've used it for a long time and I know it pretty well. I did look at ReactiveSwift, and it seems pretty straight forward. However, I decided to played it safe XP

I've used **CoreData** as the persistent storage medium. I had considered Realm (due to it's simplified interface), and even plists, SQLite. But I like Core Data, It just has so much to it and, once the boiler plate is in place, it can be relatively simple to use. Also I don't think this application will be sharing it's database so... CD it is.

Due to getting this project completed, I've left some tiny parts of it out, and left to assumptions. The testing could easily be expanded and also UI, and Snapshots have been omitted.

Another thing is I made the app as simple as possible - each file should really just need a glance to totally absorb it.

Any way, I think that's the most relevant information.

Thanks for the test guys, I really enjoyed the challenge.

Warm regards,

Ward

Installation instructions:

The project uses Cocoa pods, please run "pod install" in the terminal, then open up the wBabylonDemoProject.workspace

![alt text](https://carwad.net/sites/default/files/smiley-face-laughing-hysterically-148031-2666988.jpg)