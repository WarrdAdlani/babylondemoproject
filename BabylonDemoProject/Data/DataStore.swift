//
//  DataStore.swift
//  BabylonDemoProject
//
//  Created by Warrd Adlani on 30/06/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit
import CoreData
import PromiseKit

public enum FetchType {
    case one
    case all
}

public protocol DataStoreProtocol {
    func fetch<T: NSManagedObject>(entity: T.Type, for id: Int?, with fetchType: FetchType) -> [T]?
    func saveUsers(_ users: [User]) -> Promise<Bool>
    func savePosts(_ posts: [Post]) -> Promise<Bool>
    func saveComments(_ comments: [Comment]) -> Promise<Bool>
    func fetchUsers() -> Promise<[UserEntity]>
    func flush() -> Promise<Bool>
}

public final class DataStore: DataStoreProtocol {
    fileprivate let context: NSManagedObjectContext!
    fileprivate let persistentContainer: NSPersistentContainer!
    
    public init(container: NSPersistentContainer? = nil) {
        if let persistentContainer = container {
            persistentContainer.viewContext.automaticallyMergesChangesFromParent = true
            self.persistentContainer = persistentContainer
            self.context = persistentContainer.viewContext
            self.context.automaticallyMergesChangesFromParent = true
        } else {
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                fatalError()
            }
        appDelegate.persistentContainer.viewContext.automaticallyMergesChangesFromParent = true
            self.persistentContainer = appDelegate.persistentContainer
            self.context = appDelegate.persistentContainer.viewContext
        }
    }
    
    // MARK: Save users
    public func saveUsers(_ users: [User]) -> Promise<Bool> {
        return Promise { seal in
                    for user in users {
                        let entity = NSEntityDescription.entity(forEntityName: "UserEntity", in: self.context)!
                        let userEntity = UserEntity(entity: entity, insertInto: self.context)
                        userEntity.name = user.name
                        userEntity.id = Int16(user.id)
                        userEntity.username = user.name
                    }
                    do {
                        try self.context.save()
                        seal.fulfill(true)
                    } catch {
                        print("Failed saving")
                        seal.fulfill(false)
                    }
        }
    }
    
    // MARK: Save posts
    public func savePosts(_ posts: [Post]) -> Promise<Bool> {
        return Promise{ seal in
            for post in posts {
                let user = self.fetch(entity: UserEntity.self, for: post.userId, with: .one)
                let entity = NSEntityDescription.entity(forEntityName: "PostEntity", in: self.context)!
                let postEntity = PostEntity(entity: entity, insertInto: self.context)
                postEntity.owner = user?.first
                postEntity.id = Int16(post.id)
                postEntity.userId = Int16(post.userId)
                postEntity.title = post.title
                postEntity.body = post.title
            }
            do {
                try self.context.save()
                seal.fulfill(true)
            } catch {
                seal.fulfill(false)
            }
        }
    }
    
    // MARK: Save comments
    public func saveComments(_ comments: [Comment]) -> Promise<Bool> {
        return Promise { seal in
            for comment in comments {
                let entity = NSEntityDescription.entity(forEntityName: "CommentEntity", in: self.context)!
                let commentEntity = CommentEntity(entity: entity, insertInto: self.context)
                guard let post = self.fetch(entity: PostEntity.self, for: comment.postId, with: .one)?.first else {
                    throw NSError(domain: "failed to get post", code: 1, userInfo: nil)
                }
                commentEntity.owner = post
                commentEntity.id = Int16(post.id)
                commentEntity.postId = Int16(comment.postId)
            }
            do {
                try self.context.save()
                seal.fulfill(true)
            } catch {
                seal.fulfill(false)
            }
        }
    }
    
    // MARK: - Helper methods
    public func fetch<T: NSManagedObject>(entity: T.Type, for id: Int?, with fetchType: FetchType = .all) -> [T]? {
        let className = NSStringFromClass(entity.self).components(separatedBy: ".").last!
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: className)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
        fetchRequest.returnsObjectsAsFaults = false
        if fetchType == .one {
            guard let id = id else { fatalError("id is nil") }
            fetchRequest.predicate = NSPredicate.init(format: "id == %lu", id)
            fetchRequest.fetchLimit = 1
        }
        
        let fetchResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        try? fetchResultsController.performFetch()
        
        guard let result = fetchResultsController.fetchedObjects else {
            return nil
        }
        print("Result: \(result)")
        return result as? [T]
    }
    
    public func fetchUsers() -> Promise<[UserEntity]> {
        return Promise<[UserEntity]> { seal in
            guard let users = self.fetch(entity: UserEntity.self, for: nil, with: .all) else {
                let error = NSError(domain: "failed to load users", code: 0, userInfo: nil)
                seal.reject(error)
                return
            }
            if users.count == 0 {
                let error = NSError(domain: "no users found", code: 1, userInfo: nil)
                seal.reject(error)
            } else {
                seal.fulfill(users)
            }
        }
    }
    
    public func flush() -> Promise<Bool> {
        return deleteAll(UserEntity.self)
    }
    
    // MARK: Delete all method (should cause cascade deletion i.e. delete related objects)
    func deleteAll<T: NSManagedObject>(_ entity: T.Type) -> Promise<Bool> {
        // Early cancel if there's nothing to delete
        if fetch(entity: entity, for: nil, with: .all)?.count == 0 {
            return Promise<Bool>.value(true)
        } else {
            return Promise { seal in
                seal.fulfill(true)
                let entityName = NSStringFromClass(entity).components(separatedBy: ".").last
                let deleteFetch = NSFetchRequest<NSFetchRequestResult>.init(entityName: entityName!)
                do {
                    /* Batch deletes are ~not~ compatible with in memory CD,
                     * so run sequential deletions
                     */
                    if ProcessInfo.processInfo.environment["XCTestConfigurationFilePath"] != nil {
                        let items = try context.fetch(deleteFetch) as! [NSManagedObject]
                        
                        for item in items {
                            context.delete(item)
                        }
                    } else {
                        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
                        try! context.execute(deleteRequest)
                    }
                    
                    try context.save()
                    seal.fulfill(true)
                }
                catch let error as NSError {
                    seal.reject(error)
                }
            }
        }
    }
}
