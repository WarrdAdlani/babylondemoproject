//
//  UserEntity+CoreDataProperties.swift
//  BabylonDemoProject
//
//  Created by Warrd Adlani on 01/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//
//

import Foundation
import CoreData

extension UserEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserEntity> {
        return NSFetchRequest<UserEntity>(entityName: "UserEntity")
    }

    @NSManaged public var id: Int16
    @NSManaged public var name: String?
    @NSManaged public var username: String?
    @NSManaged public var posts: NSSet?

}

// MARK: Generated accessors for posts
extension UserEntity {

    @objc(addPostsObject:)
    @NSManaged public func addToPosts(_ value: PostEntity)

    @objc(removePostsObject:)
    @NSManaged public func removeFromPosts(_ value: PostEntity)

    @objc(addPosts:)
    @NSManaged public func addToPosts(_ values: NSSet)

    @objc(removePosts:)
    @NSManaged public func removeFromPosts(_ values: NSSet)

}
