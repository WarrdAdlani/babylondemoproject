//
//  PostEntity+CoreDataProperties.swift
//  BabylonDemoProject
//
//  Created by Warrd Adlani on 01/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//
//

import Foundation
import CoreData

extension PostEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PostEntity> {
        return NSFetchRequest<PostEntity>(entityName: "PostEntity")
    }

    @NSManaged public var body: String?
    @NSManaged public var id: Int16
    @NSManaged public var title: String?
    @NSManaged public var userId: Int16
    @NSManaged public var comments: NSSet?
    @NSManaged public var owner: UserEntity?

}

// MARK: Generated accessors for comments
extension PostEntity {

    @objc(addCommentsObject:)
    @NSManaged public func addToComments(_ value: CommentEntity)

    @objc(removeCommentsObject:)
    @NSManaged public func removeFromComments(_ value: CommentEntity)

    @objc(addComments:)
    @NSManaged public func addToComments(_ values: NSSet)

    @objc(removeComments:)
    @NSManaged public func removeFromComments(_ values: NSSet)

}
