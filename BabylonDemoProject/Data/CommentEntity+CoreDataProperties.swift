//
//  CommentEntity+CoreDataProperties.swift
//  BabylonDemoProject
//
//  Created by Warrd Adlani on 01/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//
//

import Foundation
import CoreData

extension CommentEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CommentEntity> {
        return NSFetchRequest<CommentEntity>(entityName: "CommentEntity")
    }

    @NSManaged public var id: Int16
    @NSManaged public var postId: Int16
    @NSManaged public var owner: PostEntity?

}
