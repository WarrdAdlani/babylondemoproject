//
//  Presenter.swift
//  BabylonDemoProject
//
//  Created by Warrd Adlani on 01/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation

class Presenter {
    fileprivate var interactor: UsersInputInteractorProtocol?
    fileprivate var router: UsersRouterProtocol?
    fileprivate var view: UsersViewProtocol?
}
