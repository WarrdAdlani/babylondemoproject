//
//  PostsPresenter.swift
//  BabylonDemoProject
//
//  Created by Warrd Adlani on 29/06/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit
import PromiseKit

final class PostsPresenter: PostsPresenterProtocol {
    fileprivate weak var view: PostsViewProtocol?
    fileprivate var router: PostsRouterProtocol?
    fileprivate var posts: [PostEntity]? {
        return user?.posts?.sortedArray(using: [NSSortDescriptor.init(key: "id", ascending: true)]) as? [PostEntity]
    }
    fileprivate var user: UserEntity?
    
    var isPopulated: Bool {
        let count = posts?.count ?? 0
        return count > 0
    }
    var numberOfRows: Int {
        return posts?.count ?? 0
    }
    var numberOfSections: Int {
        return 1
    }
    
    init(user: UserEntity, view: PostsViewProtocol, router: PostsRouterProtocol) {
        self.user = user
        self.view = view
        self.router = router
    }
    
    func viewDidLoad() {
        view?.showPosts()
    }
    
    func setupCell(_ cell: PostsCellPresentable, with index: Int) {
        guard let post = posts?[index] else {
            fatalError("post cannot be nil")
        }
        cell.setupCell(with: post)
    }
    
    func showPostDetail(context: UIViewController, with index: Int) {
        guard let post = posts?[index], let user = user else {
            fatalError("post cannot be nil")
        }
        router?.pushPostDetails(context: context, post: post, for: user)
    }
}
