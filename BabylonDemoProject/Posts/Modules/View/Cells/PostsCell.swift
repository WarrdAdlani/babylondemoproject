//
//  PostsTableCell.swift
//  BabylonDemoProject
//
//  Created by Warrd Adlani on 29/06/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit

// MARK: - Cell protocol
protocol PostsCellPresentable {
    func setupCell(with model: PostEntity)
}

class PostsCell: UITableViewCell {
    fileprivate var model: PostEntity? {
        didSet {
            titleLabel.text = model?.title
            bodyLabel.text = model?.body
        }
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

// MARK: - Cell setup
extension PostsCell: PostsCellPresentable {
    func setupCell(with model: PostEntity) {
        self.model = model
    }
}
