//
//  PostsViewController.swift
//  BabylonDemoProject
//
//  Created by Warrd Adlani on 29/06/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit

class PostsViewController: UIViewController, PostsViewProtocol {
  
    // MARK: IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: Member variables
    var presenter: PostsPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Posts"
        
        tableView.tableFooterView = UIView()
        tableView.register(UINib.init(nibName: PostsCell.className(), bundle: .main), forCellReuseIdentifier: PostsCell.className())
        
        presenter.viewDidLoad()
    }
    
    func showPosts() {
        tableView.reloadData()
    }
}

// MARK: TableView Delegates
extension PostsViewController: UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return presenter.numberOfSections
    }
}

// MARK: TableView Data Source
extension PostsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PostsCell.className()) as? PostsCellPresentable else {
            fatalError("Could not dequeu PostsTableCell")
        }
        
        presenter.setupCell(cell, with: indexPath.row)
        
        return cell as! UITableViewCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.showPostDetail(context: self, with: indexPath.row)
    }
}
