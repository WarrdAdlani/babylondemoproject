//
//  PostsRouter.swift
//  BabylonDemoProject
//
//  Created by Warrd Adlani on 29/06/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit

class PostsRouter: PostsRouterProtocol {
    // MARK: - Module creation method
    func createModule(context: UIViewController?, user: UserEntity) -> UIViewController? {
        let viewController: PostsViewProtocol = PostsViewController.init(nibName: "PostsViewController", bundle: .main)
        var presenter: PostsPresenterProtocol
        
        presenter = PostsPresenter(user: user, view: viewController, router: self)
        
        viewController.presenter = presenter
        
        return viewController as? UIViewController
    }
    
    // MARK: - Post details view push method
    func pushPostDetails(context: UIViewController?, post: PostEntity, for user: UserEntity) {
        let router = PostDetailsRouter()
        guard let viewController = router.createModule(context: context, with: post, and: user) else {
            fatalError()
        }
        context?.navigationController?.pushViewController(viewController, animated: true)
    }
}
