//
//  PostsProtocols.swift
//  BabylonDemoProject
//
//  Created by Warrd Adlani on 29/06/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit
import PromiseKit

// MARK: - Router protocol
protocol PostsRouterProtocol: class {
    func pushPostDetails(context: UIViewController?, post: PostEntity, for user: UserEntity)
    func createModule(context: UIViewController?, user: UserEntity) -> UIViewController?
}

// MARK: - View protocol
protocol PostsViewProtocol: class {
    var presenter: PostsPresenterProtocol! { get set }
    
    func showPosts()
}

// MARK: - Presenter protocol
protocol PostsPresenterProtocol: class {
    var numberOfRows: Int { get }
    var numberOfSections: Int { get }
    var isPopulated: Bool { get }
    
    func viewDidLoad()
    func setupCell(_ cell: PostsCellPresentable, with index: Int)
    func showPostDetail(context: UIViewController, with index: Int)
}
