//
//  WebService.swift
//  BabylonDemoProject
//
//  Created by Warrd Adlani on 29/06/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation
import PromiseKit

public protocol WebServiceProtocol {}

// MARK: NetworkManager
class WebService: WebServiceProtocol {
    var dispatcher: NetworkDispatcherProtocol!
    
    public init(dispatcher: NetworkDispatcherProtocol = NetworkDispatcher()) {
        self.dispatcher = dispatcher
    }
}

extension WebService {
    func getUsers() -> Promise<[User]> {
        return Promise { seal in
            let request = GetUsers()
            request.execute(dispatcher: dispatcher, onSuccess: { (users : [User]) in
                seal.resolve(.fulfilled(users))
            }) { (error: Error) in
                seal.reject(error)
            }
        }
    }
    
    func getPosts() -> Promise<[Post]> {
        return Promise { seal in
            let request = GetPosts()
            request.execute(dispatcher: dispatcher, onSuccess: { (posts : [Post]) in
                seal.resolve(.fulfilled(posts))
            }) { (error: Error) in
                seal.reject(error)
            }
        }
    }
    
    func getComments() -> Promise<[Comment]> {
        return Promise { seal in
            let request = GetComments()
            request.execute(dispatcher: dispatcher, onSuccess: { (posts : [Comment]) in
                seal.resolve(.fulfilled(posts))
            }) { (error: Error) in
                seal.reject(error)
            }
        }
    }
}

// MARK: Requests
struct GetPosts: RequestTypeProtocol {
    typealias ResponseType = [Post]
    var data: Request {
        return Request(path: EndPoints.posts.rawValue)
    }
}

struct GetUsers: RequestTypeProtocol {
    typealias ResponseType = [User]
    var data: Request {
        return Request(path: EndPoints.users.rawValue)
    }
}

struct GetComments: RequestTypeProtocol {
    typealias ResponseType = [Comment]
    var data: Request {
        return Request(path: EndPoints.comments.rawValue)
    }
}
