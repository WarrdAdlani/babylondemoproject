//
//  EndPoints.swift
//  BabylonDemoProject
//
//  Created by Warrd Adlani on 29/06/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation

// MARK: EndPointType enum
public enum EndPoints: String {
    /* Replace with actual endpoints */
    case posts = "http://jsonplaceholder.typicode.com/posts"
    case users = "http://jsonplaceholder.typicode.com/users"
    case comments = "http://jsonplaceholder.typicode.com/comments"
}
