//
//  Post.swift
//  BabylonDemoProject
//
//  Created by Warrd Adlani on 29/06/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

/* Used for json parsing */

import Foundation

public class Post: Codable {
    var userId: Int
    var id: Int
    var title: String
    var body: String
    var comments: [Comment]?
    
    init(id: Int, userId: Int, title: String, body: String) {
        self.id = id
        self.userId = userId
        self.title = title
        self.body = body
    }
}
