//
//  UsersInteractor.swift
//  BabylonDemoProject
//
//  Created by Warrd Adlani on 29/06/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation
import PromiseKit

final class UserInteractor: UsersInputInteractorProtocol {
    weak var presenter: UsersOutputInteractorProtocol?
    var service: WebService?
    var dataStore: DataStoreProtocol?
    
    init(dataStore: DataStoreProtocol) {
        self.dataStore = dataStore
    }
    
    func getUserAndPosts() {
        guard let service = service else {
            fatalError("service is nil")
        }
        
        guard let dataStore = dataStore else {
            return
        }
        
        /* Should fail if offline immediately and execute first catch then hit
         * ensure and try to fetch/load offline content.
         */
        
        firstly {
            when(fulfilled: service.getUsers(), service.getPosts(), service.getComments()).done { users, posts, comments in
                _ = firstly {
                        dataStore.flush()
                    }.then { _ in
                        dataStore.saveUsers(users)
                    }.then { _ in
                        dataStore.savePosts(posts)
                    }.then { _ in
                        dataStore.saveComments(comments)
                    }
                }.ensure {
                    dataStore.fetchUsers().done { users in
                        self.presenter?.didGetUsersAndPosts(users)
                        }.catch { error in
                            self.presenter?.didReceiveError(error: error)
                        }
                }
            }.catch{ (error: Error) in
                if error.localizedDescription != "The Internet connection appears to be offline." {
                    self.presenter?.didReceiveError(error: error)
                }
        }
    }
}
