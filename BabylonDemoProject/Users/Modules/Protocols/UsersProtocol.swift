//
//  UsersProtocol.swift
//  BabylonDemoProject
//
//  Created by Warrd Adlani on 29/06/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit

// MARK: - Router protocol
protocol UsersRouterProtocol: class {
    func pushPostsView(context: UIViewController, with user: UserEntity)
    func createModule(context: UIViewController?) -> UIViewController?
}

// MARK: - View protocol
protocol UsersViewProtocol: class {
    var presenter: UsersPresenterProtocol! { get set }
    
    func showUsers()
    func showError(_ error: Error)
}

// MARK: - Presenter protocol
protocol UsersPresenterProtocol: class {
    var numberOfSections: Int { get }
    var numberOfRows: Int { get }
    
    func viewDidLoad()
    func showUsers()
    func refresh()
    func setupCell(_ cell: UserCellPresentable, with index: Int)
    func showPost(context: UIViewController, with index: Int)
}

// MARK: - Input protocol
protocol UsersInputInteractorProtocol: class {
    var dataStore: DataStoreProtocol? { get set }
    var presenter: UsersOutputInteractorProtocol? { get set }
    var service: WebService? { get set }
    
    func getUserAndPosts()
}

// MARK: - Output protocol
protocol UsersOutputInteractorProtocol: class {
    func didReceiveError(error: Error)
    func didGetUsersAndPosts(_ users: [UserEntity]?)
}
