//
//  UsersRouter.swift
//  BabylonDemoProject
//
//  Created by Warrd Adlani on 29/06/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit

class UsersRouter: UsersRouterProtocol {
    
    // MARK: - Module creation
    func createModule(context: UIViewController?) -> UIViewController? {
        let viewController: UsersViewProtocol = UsersViewController.init(nibName: "UsersViewController", bundle: .main)

        let dataStore = DataStore()
        let interactor = UserInteractor(dataStore: dataStore)
        let service = WebService()
        
        let presenter: UsersPresenterProtocol & UsersOutputInteractorProtocol
        presenter = UsersPresenter(interactor: interactor, router: self, view: viewController)
        
        interactor.presenter = presenter
        interactor.service = service
        
        
        viewController.presenter = presenter
        return viewController as? UIViewController
    }
    
    // MARK: - Posts view
    func pushPostsView(context: UIViewController, with user: UserEntity) {
        let postsRouter = PostsRouter()
        
        guard let viewController = postsRouter.createModule(context: context, user: user) else {
            fatalError("Could not initialize PostsViewController")
        }
        context.navigationController?.pushViewController(viewController, animated: true)
    }
}
