//
//  UserCell.swift
//  BabylonDemoProject
//
//  Created by Warrd Adlani on 29/06/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit

// MARK: - Cell protocol
protocol UserCellPresentable {
    func setupCell(with model: UserEntity)
}

public class UserCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    
    fileprivate var model: UserEntity? {
        didSet {
            nameLabel.text = model?.username
        }
    }
}

// MARK: - Cell setup
extension UserCell: UserCellPresentable {
    func setupCell(with model: UserEntity) {
        self.model = model
    }
}
