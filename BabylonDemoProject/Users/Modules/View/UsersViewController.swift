//
//  UsersViewController.swift
//  BabylonDemoProject
//
//  Created by Warrd Adlani on 29/06/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit

class UsersViewController: UIViewController, UsersViewProtocol {

    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK: Member variables
    var presenter: UsersPresenterProtocol!
    private let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Users"

        tableView.tableFooterView = UIView()
        tableView.register(UINib.init(nibName: UserCell.className(), bundle: .main), forCellReuseIdentifier: UserCell.className())
        refreshControl.addTarget(self, action: #selector(refresh), for: .allEvents)
        tableView.addSubview(refreshControl)
        presenter.viewDidLoad()
    }
    
    func showUsers() {
        activityIndicator.stopAnimating()
        tableView.reloadData()
        if refreshControl.isRefreshing {
            refreshControl.endRefreshing()
        }
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func showError(_ error: Error) {
        UIAlertController.showAlert(context: self, error: error)
        activityIndicator.stopAnimating()
        if refreshControl.isRefreshing {
            refreshControl.endRefreshing()
        }
    }
    
    @objc func refresh() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        presenter.refresh()
    }
}

// MARK: TableView Delegates
extension UsersViewController: UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return presenter.numberOfSections
    }
}

// MARK: TableView Data Source
extension UsersViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: UserCell.className()) as? UserCellPresentable else {
            fatalError("Could not deque UserCell")
        }
        presenter.setupCell(cell, with: indexPath.row)
        
        return cell as! UITableViewCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.showPost(context: self, with: indexPath.row)
    }
}
