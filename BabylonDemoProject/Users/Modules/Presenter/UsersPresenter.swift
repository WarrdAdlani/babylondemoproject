//
//  UsersPresenter.swift
//  BabylonDemoProject
//
//  Created by Warrd Adlani on 29/06/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit

final class UsersPresenter: UsersPresenterProtocol {
    fileprivate var interactor: UsersInputInteractorProtocol?
    fileprivate var router: UsersRouterProtocol?
    fileprivate weak var view: UsersViewProtocol?
    fileprivate var users: [UserEntity]?
    var numberOfSections: Int {
        return 1
    }
    var numberOfRows: Int {
        return users?.count ?? 0
    }
    
    init(interactor: UsersInputInteractorProtocol, router: UsersRouterProtocol, view: UsersViewProtocol) {
        self.interactor = interactor
        self.router = router
        self.view = view
    }
    
    func viewDidLoad() {
        interactor?.getUserAndPosts()
    }
    
    func showUsers() {
        view?.showUsers()
    }
    
    func refresh() {
        interactor?.getUserAndPosts()
    }
    
    func setupCell(_ cell: UserCellPresentable, with index: Int) {
        guard let user = users?[index] else {
            fatalError("user cannot be nil")
        }
        cell.setupCell(with: user)
    }
    
    func showPost(context: UIViewController, with index: Int) {
        guard let user = users?[index] else {
            fatalError("posts is nil")
        }
        router?.pushPostsView(context: context, with: user)
    }
}

// MARK: - Presenter output delegates
extension UsersPresenter: UsersOutputInteractorProtocol {
    func didGetUsersAndPosts(_ users: [UserEntity]?) {
        guard let users = users else {
            return
        }
        self.users = users
        showUsers()
    }
    
    func didReceiveError(error: Error) {
        view?.showError(error)
    }
}
