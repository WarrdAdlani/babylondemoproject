//
//  User.swift
//  BabylonDemoProject
//
//  Created by Warrd Adlani on 29/06/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

/* Used for json parsing */

import Foundation

// MARK: - Codable user class
public class User: Codable {
    var id: Int
    var name: String
    var userName: String
    var posts: [Post]?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case userName = "username"
    }
    
    init(id: Int, name: String, userName: String) {
        self.id = id
        self.name = name
        self.userName = userName
    }
}
