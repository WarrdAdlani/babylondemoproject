//
//  PostDetailsPresenter.swift
//  BabylonDemoProject
//
//  Created by Warrd Adlani on 29/06/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation

final class PostDetailsPresenter: PostsDetailsPresenterProtocol {
    fileprivate var view: PostsDetailsViewProtocol?
    fileprivate var router: PostDetailsRouterProtocol?
    fileprivate var post: PostEntity?
    fileprivate var user: UserEntity?
    
    func viewDidLoad() {
        update()
    }
    
    init(user: UserEntity?, post: PostEntity?, view: PostsDetailsViewProtocol?, router: PostDetailsRouterProtocol?) {
        self.user = user
        self.post = post
        self.view = view
        self.router = router
    }
    
    func update() {
        guard let post = post, let user = user else {
            fatalError("post is nil for post details")
        }
        view?.showPostDetails(post, for: user)
    }
}
