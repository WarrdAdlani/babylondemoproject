//
//  PostDetailsProtocols.swift
//  BabylonDemoProject
//
//  Created by Warrd Adlani on 29/06/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit

// MARK: - Router protocol
protocol PostDetailsRouterProtocol: class {
    func createModule(context: UIViewController?, with post: PostEntity, and user: UserEntity) -> UIViewController?
}

// MARK: - View protocol
protocol PostsDetailsViewProtocol: class {
    var presenter: PostsDetailsPresenterProtocol! { get set }
    
    func showPostDetails(_ post: PostEntity, for user: UserEntity)
}

// MARK: - Presenter protocol
protocol PostsDetailsPresenterProtocol: class {
    func viewDidLoad()
    func update()
}
