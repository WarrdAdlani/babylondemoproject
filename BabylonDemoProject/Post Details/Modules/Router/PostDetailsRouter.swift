//
//  PostDetailsRouter.swift
//  BabylonDemoProject
//
//  Created by Warrd Adlani on 29/06/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit

final class PostDetailsRouter: PostDetailsRouterProtocol {
    // MARK: - Module creation method
    func createModule(context: UIViewController?, with post: PostEntity, and user: UserEntity) -> UIViewController? {
        let viewController: PostsDetailsViewProtocol = PostDetailsViewController.init(nibName: "PostDetailsViewController", bundle: .main)
        var presenter: PostsDetailsPresenterProtocol
        presenter = PostDetailsPresenter(user: user, post: post, view: viewController, router: self)
        
        viewController.presenter = presenter
        
        return viewController as? UIViewController
    }
}
