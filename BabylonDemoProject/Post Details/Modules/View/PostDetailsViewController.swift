//
//  PostDetailsViewController.swift
//  BabylonDemoProject
//
//  Created by Warrd Adlani on 29/06/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit

class PostDetailsViewController: UIViewController {
    
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var commentsLabel: UILabel!
    
    var presenter: PostsDetailsPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Post"
        
        presenter.viewDidLoad()
    }
}

// MARK: - Post details view delegates
extension PostDetailsViewController: PostsDetailsViewProtocol {
    @objc func showPostDetails(_ post: PostEntity, for user: UserEntity) {
        
        authorLabel.text = user.username
        bodyLabel.text = post.body
        commentsLabel.text = "Comments: \(post.comments?.count ?? 0)"
    }
}
