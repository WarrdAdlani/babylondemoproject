//
//  UIAlertController+Extensions.swift
//  BabylonDemoProject
//
//  Created by Warrd Adlani on 29/06/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit

extension UIAlertController {
    class func showAlert(context: UIViewController, error: Error) {
        let alertController = UIAlertController.init(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        
        let okayAction = UIAlertAction(title: "Okay", style: .default, handler: nil)
        
        alertController.addAction(okayAction)
        
        context.present(alertController, animated: true, completion: nil)
    }
}
