//
//  UIView+Extensions.swift
//  BabylonDemoProject
//
//  Created by Warrd Adlani on 29/06/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit

extension NSObject {
    public class func className() -> String {
        return NSStringFromClass(self.classForCoder()).components(separatedBy: ".").last!
    }
}
