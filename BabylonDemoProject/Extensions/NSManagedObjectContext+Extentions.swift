//
//  NSManagedObjectContext+Extentions.swift
//  BabylonDemoProject
//
//  Created by Warrd Adlani on 01/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation
import CoreData

extension NSManagedObjectContext {
    
    // Immediately merges changes and updates context
    public func executeAndMergeChanges(using batchDeleteRequest: NSBatchDeleteRequest) throws {
        batchDeleteRequest.resultType = .resultTypeObjectIDs
        let result = try execute(batchDeleteRequest) as? NSBatchDeleteResult
        let changes: [AnyHashable: Any] = [NSDeletedObjectsKey: result?.result as? [NSManagedObjectID] ?? []]
        NSManagedObjectContext.mergeChanges(fromRemoteContextSave: changes, into: [self])
    }
}
