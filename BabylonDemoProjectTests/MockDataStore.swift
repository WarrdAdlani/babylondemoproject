//
//  MockDataStore.swift
//  BabylonDemoProjectTests
//
//  Created by Warrd Adlani on 02/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation
import CoreData
import BabylonDemoProject
import PromiseKit

// Mock datastore class
class MockDataStore {
    lazy var dataStore = {
        return DataStore(container: self.mockPersistantContainer)
    }()
    
    var context: NSManagedObjectContext!
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        let managedObjectModel = NSManagedObjectModel.mergedModel(from: [Bundle(for: type(of: self))] )!
        return managedObjectModel
    }()
    
    lazy var mockPersistantContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Users", managedObjectModel: self.managedObjectModel)
        let description = NSPersistentStoreDescription()
        description.type = NSInMemoryStoreType
        description.shouldAddStoreAsynchronously = false
        
        container.persistentStoreDescriptions = [description]
        container.loadPersistentStores { (description, error) in
            precondition( description.type == NSInMemoryStoreType )
            if let error = error {
                fatalError("memory coordinator failed to create: \(error)")
            }
        }
        return container
    }()
    
    func flushData() {
        _ = dataStore.flush()
    }
    
    func usersStub(completion: @escaping ([UserEntity])->()) {
        let users = [UserStub()]
        let posts = [PostStub()]
        let comments = [CommentStub()]
        _ = firstly {
            dataStore.saveUsers(users)
            }.then { _ in
                self.dataStore.savePosts(posts)
            }.then { _ in
                self.dataStore.saveComments(comments)
            }.done { _ in
                _ = self.dataStore.fetchUsers().done { users in
                    completion(users)
                    }.catch{ error in
                        fatalError(error.localizedDescription)
                }
            }.catch { error in
                fatalError(error.localizedDescription)
        }
    }
}
