//
//  DataStoreTests.swift
//  BabylonDemoProjectTests
//
//  Created by Warrd Adlani on 01/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import XCTest
import CoreData
import PromiseKit

@testable import BabylonDemoProject

class DataStoreTests: XCTestCase {
    var context: NSManagedObjectContext!

    lazy var managedObjectModel: NSManagedObjectModel = {
        let managedObjectModel = NSManagedObjectModel.mergedModel(from: [Bundle(for: type(of: self))] )!
        return managedObjectModel
    }()
    
    lazy var mockPersistantContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "Users", managedObjectModel: self.managedObjectModel)
        let description = NSPersistentStoreDescription()
        description.type = NSInMemoryStoreType
        description.shouldAddStoreAsynchronously = false
        
        container.persistentStoreDescriptions = [description]
        container.loadPersistentStores { (description, error) in
            precondition( description.type == NSInMemoryStoreType )
            if let error = error {
                fatalError("memory coordinator failed to create: \(error)")
            }
        }
        return container
    }()
    
    var dataStore: DataStore!
    
    override func setUp() {
        dataStore = DataStore(container: mockPersistantContainer)
        _ = dataStore.flush()
    }
    
    func testAddUsersToDataStore() {
        let saveExpectation = XCTestExpectation(description: "save user to core data")
        let users = [UserStub()]
        
        dataStore.saveUsers(users).done { (completed) in
                XCTAssertTrue(completed)
                saveExpectation.fulfill()
            }.catch { error in
                XCTFail()
        }
        
        wait(for: [saveExpectation], timeout: 5.0)
    }
    
    func testAddAndDeleteUsers() {
        let deleteExpectation = XCTestExpectation(description: "delete user to core data")
        let users = [UserStub()]
        
        _ = firstly {
                dataStore.saveUsers(users)
            }.then { saved in
                self.dataStore.flush()
            }.done { flushed in
                XCTAssertTrue(flushed)
                deleteExpectation.fulfill()
            }.catch { error in
                XCTFail()
        }
        
        wait(for: [deleteExpectation], timeout: 5.0)
    }
    
    func testAddUsersAndRetrieveFromDataStore() {
        let loadExpectation = XCTestExpectation(description: "load users from core data")
       
        let users = [UserStub()]
        let posts = [PostStub()]
        let comments = [CommentStub()]
        
        _ = firstly {
                dataStore.saveUsers(users)
            }.then { _ in
                self.dataStore.savePosts(posts)
            }.then { _ in
                self.dataStore.saveComments(comments)
            }.done { _ in
                _ = self.dataStore.fetchUsers().done { users in
                        XCTAssertNotNil(users)
                        let user = users.first
                        XCTAssertTrue(user?.name == "bob")
                        loadExpectation.fulfill()
                    }.catch{ error in
                        print(error)
                        XCTFail()
                }
            }.catch { error in
                print(error)
                XCTFail()
        }
        wait(for: [loadExpectation], timeout: 5.0)
    }
}

class UserStub: User  {
    init() {
        super.init(id: 1, name: "bob", userName: "bob the builder")
        self.posts = nil
    }
    
    required init(from decoder: Decoder) throws {
        fatalError("init(from:) has not been implemented")
    }
}

class PostStub: Post  {
    init() {
        super.init(id: 1, userId: 1, title: "Title of post", body: "Body of post with details")
    }
    
    required init(from decoder: Decoder) throws {
        fatalError("init(from:) has not been implemented")
    }
}

class CommentStub: Comment {
    init() {
        super.init(postId: 1, id: 1, name: "the user", email: "test@test.com", body: "The information body of a comment")
    }
    
    required init(from decoder: Decoder) throws {
        fatalError("init(from:) has not been implemented")
    }
}
