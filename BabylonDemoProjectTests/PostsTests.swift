//
//  PostsTests.swift
//  BabylonDemoProjectTests
//
//  Created by Warrd Adlani on 02/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import XCTest
import CoreData

@testable import BabylonDemoProject

class PostsTests: XCTestCase {
    
    let mockDataStore = {
        return MockDataStore()
    }()
    
    lazy var mockView: MockPostsView = {
        let mockView = MockPostsView(nibName: "PostsViewController", bundle: .main)
        _ = Bundle.main.loadNibNamed("PostsViewController", owner: mockView, options: nil)
        return mockView
    }()
    
    func testCreateModule() {
        let loadExpectation = expectation(description: "load expectation")
        let router = PostsRouter()
        mockDataStore.usersStub { users in
            let user = users.first
            let viewController = router.createModule(context: nil, user: user!)
            XCTAssertNotNil(viewController)
            loadExpectation.fulfill()
        }
        wait(for: [loadExpectation], timeout: 5.0)
    }
    
    func testViewHasPresenter() {
        let loadExpectation = expectation(description: "load expectation")
        let router = PostsRouter()
        mockDataStore.usersStub { users in
            let user = users.first
            let viewController = router.createModule(context: nil, user: user!) as? PostsViewProtocol
            XCTAssertNotNil(viewController?.presenter)
            loadExpectation.fulfill()
        }
        wait(for: [loadExpectation], timeout: 5.0)
    }
    
    func testViesCanShowPost() {
        let loadExpectation = expectation(description: "load expectation")
        let router = PostsRouter()
        mockDataStore.usersStub { users in
            let user = users.first
            let presenter = PostsPresenter(user: user!, view: self.mockView, router: router)
            self.mockView.presenter = presenter
            self.mockView.viewDidLoad()
            XCTAssertTrue(self.mockView.spyDidShowPosts)
            loadExpectation.fulfill()
        }
        wait(for: [loadExpectation], timeout: 5.0)
    }
    
    func testPresenterReturnsNumberOfSectionsAndNumberOfRows() {
        let loadExpectation = expectation(description: "load expectation")
        let mockView = MockPostsView(nibName: "PostsViewController", bundle: .main)
        _ = Bundle.main.loadNibNamed("PostsViewController", owner: mockView, options: nil)
        let router = PostsRouter()
        mockDataStore.usersStub { users in
            let user = users.first
            let presenter = PostsPresenter(user: user!, view: mockView, router: router)
            mockView.presenter = presenter
            mockView.viewDidLoad()
            XCTAssertTrue(presenter.numberOfRows == 1)
            XCTAssertTrue(presenter.numberOfSections == 1)
            loadExpectation.fulfill()
        }
        wait(for: [loadExpectation], timeout: 5.0)
    }
    
    func testPresenterIsPopulatedBoolIsTrueWhenPopulated() {
        let loadExpectation = expectation(description: "load expectation")
        
        let router = PostsRouter()
        mockDataStore.usersStub { users in
            let user = users.first
            let presenter = PostsPresenter(user: user!, view: self.mockView, router: router)
            self.mockView.presenter = presenter
            self.mockView.viewDidLoad()
            XCTAssertTrue(presenter.isPopulated)
            loadExpectation.fulfill()
        }
        wait(for: [loadExpectation], timeout: 5.0)
    }
    
    func testPresenterViewDidLoadIsCalled() {
        let loadExpectation = expectation(description: "load expectation")
        
        mockDataStore.usersStub { users in
            let user = users.first
            let presenter = MockPostsPresenter()
            presenter.user = user
            self.mockView.presenter = presenter
            self.mockView.viewDidLoad()
            XCTAssertTrue(presenter.spyViewDidLoad)
            loadExpectation.fulfill()
        }
        wait(for: [loadExpectation], timeout: 5.0)
    }
    
    func testPresenterDidSetupCell() {
        let loadExpectation = expectation(description: "load expectation")
        
        mockDataStore.usersStub { users in
            let user = users.first
            let presenter = MockPostsPresenter()
            presenter.user = user
            self.mockView.presenter = presenter
            self.mockView.viewDidLoad()
            self.mockView.tableView.reloadRows(at: [IndexPath.init(item: 0, section: 0)], with: .none)
            XCTAssertTrue(presenter.spyDidSetupCell)
            loadExpectation.fulfill()
        }
        wait(for: [loadExpectation], timeout: 5.0)
    }
    
    func testPresenterDidShowPostDetails() {
        let loadExpectation = expectation(description: "load expectation")
        let presenter = MockPostsPresenter()
        
        mockDataStore.usersStub { users in
            let user = users.first
            presenter.user = user
            self.mockView.presenter = presenter
            self.mockView.viewDidLoad()
            self.mockView.tableView.reloadRows(at: [IndexPath.init(item: 0, section: 0)], with: .none)
            self.mockView.tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: .none)
            self.mockView.tableView(self.mockView.tableView, didSelectRowAt: IndexPath(row: 0, section: 0))
            XCTAssertTrue(presenter.spyDidShowPostDetails)
            loadExpectation.fulfill()
        }
        wait(for: [loadExpectation], timeout: 5.0)
    }
}

class MockPostsView: PostsViewController {
    var spyDidShowPosts = false
    override func showPosts() {
        spyDidShowPosts = true
    }
}

class MockPostsPresenter: PostsPresenterProtocol {
    var spyDidShowPostDetails = false
    var spyViewDidLoad = false
    var spyDidSetupCell = false
    
    var user: UserEntity?
    var posts: [PostEntity]? {
        let descriptor = NSSortDescriptor(key: "id", ascending: true)
        return user?.posts?.sortedArray(using: [descriptor]) as? [PostEntity]
    }
    
    var numberOfRows: Int {
        return user?.posts?.count ?? 0
    }
    
    var numberOfSections: Int {
        return 1
    }
    
    var isPopulated: Bool {
        if let posts = posts {
            return posts.count > 0
        }
        return false
    }
    
    func showPostDetail(context: UIViewController, with index: Int) {
        spyDidShowPostDetails = true
    }
    
    func viewDidLoad() {
        spyViewDidLoad = true
    }
    
    func setupCell(_ cell: PostsCellPresentable, with index: Int) {
        spyDidSetupCell = true
    }
}
