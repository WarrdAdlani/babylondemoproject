//
//  UsersTests.swift
//  BabylonDemoProjectTests
//
//  Created by Warrd Adlani on 02/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import XCTest
import UIKit
import CoreData
import PromiseKit

@testable import BabylonDemoProject

class UsersTests: XCTestCase {
    lazy var router = {
        return UsersRouter()
    }()

    let mockDataStore = {
        return MockDataStore()
    }()
    
    lazy var userViewController: UsersViewController = {
        let vc: UsersViewProtocol = router.createModule(context: nil) as! UsersViewProtocol
    _ = UINavigationController(rootViewController: vc as! UIViewController)
    _ = Bundle.main.loadNibNamed("UsersViewController", owner: vc, options: nil)
        return vc as! UsersViewController
    }()
    
    lazy var mockView: MockView = {
        let mockView = MockView.init(nibName: "UsersViewController", bundle: .main)
        _ = Bundle.main.loadNibNamed("UsersViewController", owner: mockView, options: nil)
        return mockView
    }()
 
    override func setUp() {
        mockDataStore.flushData()
    }
    
    func testUserRouterCanCreateModule() {
        let router = UsersRouter()
        let userView: UsersViewProtocol = router.createModule(context: nil)! as! UsersViewProtocol
        XCTAssertNotNil(userView)
    }
    
    func testDidPushPostsView() {
        let loadUsersExpectations = XCTestExpectation(description: "load users expectation")
        let viewController: UsersViewController = router.createModule(context: nil) as! UsersViewController
        _ = UINavigationController(rootViewController: viewController)
        mockDataStore.usersStub { users in
            let user = users.first
            self.router.pushPostsView(context: viewController, with: user!)
            XCTAssertTrue(viewController.navigationController!.viewControllers.count == 1)
            loadUsersExpectations.fulfill()
        }
        
        wait(for: [loadUsersExpectations], timeout: 5.0)
    }
    
    func testUserViewHasPresenter() {
        let router = UsersRouter()
        let viewController: UsersViewProtocol = router.createModule(context: nil) as! UsersViewProtocol
        XCTAssertNotNil(viewController.presenter)
    }
    
    func testUserViewShowsUsers() {
        let loadUsersExpectations = XCTestExpectation(description: "show users expectation")
        
        let mockInteractor = MockUserInteractor()
        mockInteractor.mockService = MockService(isNetworkReachable: true)
        userViewController.presenter = MockUserPresenter(interactor: mockInteractor, view: userViewController, router: nil)
        let view = userViewController
        let mockPresenter = userViewController.presenter as? MockUserPresenter
        mockInteractor.presenter = mockPresenter
        view.viewDidLoad()
        
        mockPresenter?.spyShowUsersBlock = { finished in
            view.tableView.reloadData()
            XCTAssertTrue(view.presenter.numberOfRows > 0)
            XCTAssert(view.tableView.numberOfRows(inSection: 0) > 0)
            XCTAssert(view.tableView.numberOfRows(inSection: 0) == view.presenter.numberOfRows)
            loadUsersExpectations.fulfill()
        }
        wait(for: [loadUsersExpectations], timeout: 5.0)
    }
    
    func testPresenterDidInitialize() {
        XCTAssertNotNil(userViewController.presenter)
    }
    
    func testPresenterReturnsNumberOfRowsAndSections() {
        let loadUsersExpectations = XCTestExpectation(description: "show users expectation")
        
        let mockInteractor = MockUserInteractor()
        mockInteractor.mockService = MockService(isNetworkReachable: true)
        userViewController.presenter = MockUserPresenter(interactor: mockInteractor, view: userViewController, router: nil)
        let view = userViewController
        let mockPresenter = userViewController.presenter as? MockUserPresenter
        mockInteractor.presenter = mockPresenter
        view.viewDidLoad()
        
        mockPresenter?.spyShowUsersBlock = { finished in
            view.tableView.reloadData()
            XCTAssert(view.presenter.numberOfRows == 1)
            XCTAssert(view.presenter.numberOfSections == 1)
            loadUsersExpectations.fulfill()
        }
        wait(for: [loadUsersExpectations], timeout: 5.0)
    }
    
    func testPresenterDidSetupCell() {
        let loadUsersExpectations = XCTestExpectation(description: "setup cell expectation")
        let mockInteractor = MockUserInteractor()
        let mockPresenter = MockUserPresenter(interactor: mockInteractor, view: mockView, router: nil) as MockUserPresenter
        mockInteractor.mockService = MockService(isNetworkReachable: true)
        mockInteractor.presenter = mockPresenter
        mockView.presenter = mockPresenter
        mockView.viewDidLoad()
        
        mockPresenter.spyShowUsersBlock = { _ in
            self.mockView.tableView.reloadData()
            self.mockView.tableView.reloadRows(at: [IndexPath(item: 0, section: 0)], with: .none)
        }
        mockPresenter.spyDidSetupCellBlock = { success in
            XCTAssertNotNil(success)
            loadUsersExpectations.fulfill()
        }
        wait(for: [loadUsersExpectations], timeout: 5.0)
    }
    
    func testPresenterDidShowPost() {
        let loadUsersExpectations = XCTestExpectation(description: "show post  expectation")
        let view = mockView
        let router = UsersRouter()
        let mockInteractor = MockUserInteractor()
        let mockPresenter: MockUserPresenter = MockUserPresenter(interactor: mockInteractor, view: view, router: router)
        mockInteractor.mockService = MockService(isNetworkReachable: true)
        userViewController.presenter = mockPresenter
        
        mockInteractor.presenter = mockPresenter
        mockView.presenter = mockPresenter
        mockView.viewDidLoad()
        
        mockPresenter.spyShowUsersBlock = { users in
            let user = users.first
            let viewController = self.userViewController as UIViewController
            
            self.router.pushPostsView(context: viewController, with: user!)
            loadUsersExpectations.fulfill()
        }
        wait(for: [loadUsersExpectations], timeout: 5.0)
    }
    
    func testPresenterDidShowError() {
        let loadUsersExpectations = XCTestExpectation(description: "show error expectation")
        let mockInteractor = MockUserInteractor()
        let mockPresenter = MockUserPresenter(interactor: mockInteractor, view: mockView, router: nil) as MockUserPresenter
        mockInteractor.mockService = MockService(isNetworkReachable: false)
        mockInteractor.presenter = mockPresenter
        mockPresenter.spyShowErrorBlock = { _ in
            XCTAssertTrue(self.mockView.spyDidShowError)
            loadUsersExpectations.fulfill()
        }
        mockView.presenter = mockPresenter
        mockView.viewDidLoad()
        wait(for: [loadUsersExpectations], timeout: 5.0)
    }
    
    func testInteractorGetUsersAndPosts() {
        let loadUsersExpectations = XCTestExpectation(description: "get users expectation")
        let mockInteractor = MockUserInteractor()
        let view = userViewController
        var mockPresenter = userViewController.presenter as? MockUserPresenter
        
        mockPresenter = MockUserPresenter(interactor: mockInteractor, view: view, router: nil)
        mockInteractor.mockService = MockService(isNetworkReachable: true)
        userViewController.presenter = MockUserPresenter(interactor: mockInteractor, view: userViewController, router: nil)
        mockInteractor.presenter = mockPresenter
        
        mockPresenter?.interactor?.spyUserBlock = { users in
            XCTAssertTrue(users.count == 1)
            let  user = users.first
            XCTAssertNotNil(user?.posts)
            loadUsersExpectations.fulfill()
        }
        
        view.viewDidLoad()
        wait(for: [loadUsersExpectations], timeout: 5.0)
    }
    
    func testInteractorGetUsersAndPostsFail() {
        let loadUsersExpectations = XCTestExpectation(description: "fail getting users expectation")
        let mockInteractor = MockUserInteractor()
        let view = userViewController
        var mockPresenter = userViewController.presenter as? MockUserPresenter
        
        mockPresenter = MockUserPresenter(interactor: mockInteractor, view: view, router: nil)
        mockInteractor.mockService = MockService(isNetworkReachable: false) // <- false
        userViewController.presenter = MockUserPresenter(interactor: mockInteractor, view: userViewController, router: nil)
        mockInteractor.presenter = mockPresenter
        
        mockPresenter?.interactor?.spyErrorBlock = { error in
            XCTAssertTrue(error.localizedDescription.contains("Fake error"))
            loadUsersExpectations.fulfill()
        }
        
        view.viewDidLoad()
        wait(for: [loadUsersExpectations], timeout: 5.0)
    }
}

class MockUserPresenter: UsersPresenterProtocol {
    var spyShowUsersBlock: (([UserEntity])->())?
    var spyDidSetupCellBlock: ((Bool)->())?
    var spyShowErrorBlock: ((Error)->())?
    
    var users: [UserEntity]?
    var numberOfSections: Int = 1
    var numberOfRows: Int {
        return users?.count ?? 0
    }
    var interactor: MockUserInteractor?
    var view: UsersViewProtocol?
    var router: UsersRouter?
    
    init(interactor: MockUserInteractor?, view: UsersViewProtocol?, router: UsersRouter?) {
        self.interactor = interactor
        self.view = view
        self.router = router
    }
    
    func viewDidLoad() {
        interactor?.getUserAndPosts()
    }
    
    func showUsers() {
        view?.showUsers()
    }
    
    func refresh() { }
    func setupCell(_ cell: UserCellPresentable, with index: Int) {
        let user = users?[index]
        cell.setupCell(with: user!)
        spyDidSetupCellBlock?(true)
    }
    func showPost(context: UIViewController, with index: Int) { }
}

extension MockUserPresenter: UsersOutputInteractorProtocol {
    func didReceiveError(error: Error) {
        view?.showError(error)
        spyShowErrorBlock?(error)
    }
    
    func didGetUsersAndPosts(_ users: [UserEntity]?) {
        self.users = users
        showUsers()
        spyShowUsersBlock?(users!)
    }
}

class MockUserInteractor: UsersInputInteractorProtocol {
    var spyUserBlock: (([UserEntity])->())?
    var spyErrorBlock: ((Error)->())?
    
    var service: WebService?
    var dataStore: DataStoreProtocol?
    var presenter: UsersOutputInteractorProtocol?
    var mockService: MockService?
    lazy var mockDataStore = {
        return MockDataStore()
    }()
    
    func getUserAndPosts() {
        mockService?.makeRequest(completion: { isNetworkReachable in
            let error = NSError(domain: "Fake error", code: 0, userInfo: nil)
            if isNetworkReachable == false{
                if let block = spyErrorBlock {
                    block(error)
                } else {
                    presenter?.didReceiveError(error: error)
                }
            } else {
                mockDataStore.usersStub { users in
                    if let block = self.spyUserBlock {
                        block(users)
                    } else {
                        self.presenter?.didGetUsersAndPosts(users)
                    }
                }
            }
        })
    }
}

class MockService: WebService {
    var isNetworkReachable: Bool
    init(isNetworkReachable: Bool = true) {
        self.isNetworkReachable = isNetworkReachable
    }
    func makeRequest(completion: (_ isNetworkReachable: Bool)->()) {
        completion(isNetworkReachable)
    }
}

class MockView: UsersViewController {
    var spyDidShowError = false
    var spyDidShowUsers = false
    
    override func showUsers() {
        spyDidShowUsers = true
    }
    
    override func showError(_ error: Error) {
        spyDidShowError = true
    }
}

