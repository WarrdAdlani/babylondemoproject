//
//  PostDetailsTests.swift
//  BabylonDemoProjectTests
//
//  Created by Warrd Adlani on 03/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import XCTest
import CoreData

@testable import BabylonDemoProject

class PostDetailsTests: XCTestCase {
    let mockDataStore = {
        return MockDataStore()
    }()
    
    lazy var mockView: MockPostDetailsView = {
        let mockView = MockPostDetailsView(nibName: "PostDetailsViewController", bundle: .main)
        _ = Bundle.main.loadNibNamed("PostDetailsViewController", owner: mockView, options: nil)
        return mockView
    }()
    
    func testPostDetailsPresenterIsInitialzed() {
        let loadExpectation = XCTestExpectation(description: "load expectation")
        let router = PostDetailsRouter()
        
        mockDataStore.usersStub { users in
            let user = users.first!
            let descriptor = NSSortDescriptor(key: "id", ascending: true)
            let post = user.posts?.sortedArray(using: [descriptor]).first as! PostEntity
            let view = router.createModule(context: nil, with: post, and: user) as? PostsDetailsViewProtocol
            XCTAssertNotNil(view?.presenter)
            loadExpectation.fulfill()
        }
        wait(for: [loadExpectation], timeout: 3.0)
    }
    
    func testViewShowsPostDetails() {
        let loadExpectation = XCTestExpectation(description: "load expectation")
        
        mockDataStore.usersStub { users in
            let user = users.first!
            let descriptor = NSSortDescriptor(key: "id", ascending: true)
            let post = user.posts?.sortedArray(using: [descriptor]).first as! PostEntity
            
            let mockPresenter = MockPostDetailsPresenter(user: user, post: post, view: self.mockView)
            self.mockView.presenter = mockPresenter
            self.mockView.viewDidLoad()
            XCTAssertTrue(mockPresenter.spyViewDidUpdate)
            loadExpectation.fulfill()
        }
        wait(for: [loadExpectation], timeout: 3.0)
    }
}

class MockPostDetailsPresenter: PostsDetailsPresenterProtocol {
    var spyViewDidLoad = false
    var spyViewDidUpdate = false
    
    fileprivate var view: PostsDetailsViewProtocol?
    fileprivate var router: PostDetailsRouterProtocol?
    fileprivate var post: PostEntity?
    fileprivate var user: UserEntity?
    
    init(user: UserEntity, post: PostEntity, view: PostsDetailsViewProtocol) {
        self.user = user
        self.post = post
        self.view = view
    }
    
    func viewDidLoad() {
        spyViewDidLoad = true
        update()
    }
    
    func update() {
       spyViewDidUpdate = true
    }
}

class MockPostDetailsView: PostDetailsViewController {
    var spyViewDidLoad = false
    var spyDidShowPostDetails = false
    
    override func viewDidLoad() {
        spyViewDidLoad = true
        presenter.viewDidLoad()
    }
}

extension MockPostDetailsView {
    override func showPostDetails(_ post: PostEntity, for user: UserEntity) {
        spyDidShowPostDetails = true
    }
}
